package cc.gemii.utils;

import java.security.MessageDigest;

public class MD5Utils {

	/***
	 * MD5加密 生成32位md5码
	 * 
	 * @param 待加密字符串
	 * @return 返回32位md5码
	 */
	public static String md5Encode(String inStr) {
		try {
			MessageDigest md5 = null;
			try {
				md5 = MessageDigest.getInstance("MD5");
			} catch (Exception e) {
				System.out.println(e.toString());
				e.printStackTrace();
				return "";
			}

			byte[] byteArray = inStr.getBytes("UTF-8");
			byte[] md5Bytes = md5.digest(byteArray);
			StringBuffer hexValue = new StringBuffer();
			for (int i = 0; i < md5Bytes.length; i++) {
				int val = ((int) md5Bytes[i]) & 0xff;
				if (val < 16) {
					hexValue.append("0");
				}
				hexValue.append(Integer.toHexString(val));
			}
			return hexValue.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inStr;
	}
	
	public static void main(String[] args) {
		String startTimeStr = "2017-03-30 00:00:00";
		String endTimeStr = "2017-04-01 00:00:00";
		String originStr = "leqeeGemii" + startTimeStr + endTimeStr + "65617I6$nGY";
		String MD5Str = MD5Utils.md5Encode(originStr);
		System.out.println(MD5Str);
	}
}
