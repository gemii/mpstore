package cc.gemii.utils;

public class UrlConsts {

	public static final String ACCESS_ALLOW_ORIGIN = "https://mp.gemii.cc";

	public static final String GET_OPENID_SK_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=MINI_APP_ID&secret=MINI_APP_SECRET&js_code=MINI_CODE&grant_type=authorization_code";
}
