package cc.gemii.utils;


import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;


/**
 * @description 常用工具类
 * @author      xudj
 * @version     1.0
 */
public final class CommonUtil {

	/**
	 * 性别的数据字典对应的代码	
	 */
	public static final String DICT_CODE_SEX="sex";
	
	/**
	 * 月份
	 */
	public static final String[] monthArray={"一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"};
	/**当前日期-示例： 151212*/
	public static final SimpleDateFormat dateFormat_yyMMdd=new SimpleDateFormat("yyMMdd");
	/**当前日期-示例： 20151212*/
	public static final SimpleDateFormat dateFormat_yyyyMMdd=new SimpleDateFormat("yyyyMMdd");						
	/**当前日期-示例： 20151212080808*/
	public static final SimpleDateFormat dateFormat_yyyyMMddHHmmss=new SimpleDateFormat("yyyyMMddHHmmss");			
	/**当前日期-示例： 2015-12-12*/
	public static final SimpleDateFormat dateFormat_yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
	/**当前日期-示例： 2015.12.12*/
	public static final SimpleDateFormat dateFormat_yyyy$MM$dd = new SimpleDateFormat("yyyy.MM.dd");
	/**当前日期-示例： 2015-12-12 12:12:12*/
	public static final SimpleDateFormat dateFormat_yyyy_MM_ddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**当前日期-示例： 2015-12-12 12:12*/
	public static final SimpleDateFormat dateFormat_yyyy_MM_ddHHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	/**当前日期-示例： 2015/12/12 12:12*/
	public static final SimpleDateFormat dateFormat_yyyy$MM$ddHHmm = new SimpleDateFormat("yyyy/MM/dd HH:mm");	
	
	
	/**
     * 日期转换
     * @author xudj20170206
     * @param date
     * @param format:时间类型
     * @return
     */
    public static String dateFormat(Date date, SimpleDateFormat sdf){
    	try{
    		if(date == null){
	    		return null;
	    	}
    		synchronized(sdf){
    			return sdf.format(date);
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return null;
    }
    
   /**
    * 字符串转化为日期类型
    * @author xudj20170217
    * @param  日期类型
    * @return yyyy-MM-dd类型的日期
    * @throws
     */
    public static Date String2Date(String string, SimpleDateFormat sdf){
    	if(!CommonUtil.hasText(string)){
    		return null;
    	}
    	Date date = new Date();
		try {
			synchronized(sdf){
				date = sdf.parse(string);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    return date;
    }
	
	/**
	 * @description 验证字符串是否为空，为空时返回false不为空时返回true
	 * @author xudj
	 * @param str
	 * @return
	 */
	public static boolean hasText(Object str){
		//如果是null,返回false;
		if(null==str){
			return false;
		}else {
			//如果不是null，判断是否是“null”或""
			if (("".equals((str+"").trim()))||("null".equals((str+"").trim()))) {
				//如果是"null"或""返回false
				return false;
			}else {
				//如果不是null也不是"null"或""返回true
				return true;
			}
		}
	}
	
	/**
	 * @description 格式化手机号
	 * @author xudj
	 * @param str
	 * @return
	 */
	public static String getSafeMobile(Object str){
		//如果是null,返回false;
		if(hasText(str)){
			String mobile=str.toString();
			if (mobile.length()==11) {
				return mobile.substring(0, 3)+"****"+mobile.substring(7);
			}
			return "非法手机号码";
		}
		return "未知";
	}
	
	/**
	 * 格式化身份证号
	 * @returntype 	String 格式化后的身份证号 like this 1100 ***** ***** 1438
	 */
	public static String getSafeIdCard(Object str){
		if(CommonUtil.hasText(str)){
			String cardNo=str.toString();
			if(cardNo.length()>=12){
				return cardNo.substring(0,4)+" ***** ***** "+cardNo.substring(cardNo.length()-4);
			}else{
				return cardNo;
			}
		}
		return "卡号为空";
	}
	
	/**
	 * @description 转义特殊字符（如果需要返回json数据，换行等特殊字符会导致json数据换行导致解析错误）
	 * @author xudj
	 * @param str
	 * @return
	 */
	public static String decodeText(String str){
	          String s = "";
	          if (!CommonUtil.hasText(str)) {
				return "";
			}
	         s = str.replace("&amp;","&");
	          s = s.replace("&lt;","<");
	         s = s.replace("&gt;",">");
	          s = s.replace("&nbsp;"," ");
	          s = s.replace("&#39;","\'");
	          s = s.replace("&quot;","\"");
	          return s;  
	}
	
	/**
	 * @description 生成6位验证码
	 * @author xudj
	 * @return
	 */
	public static synchronized String getCaptcha() {
		String captcha = "";
		
		for(int i = 0;i < 6;i++)
		{
			captcha += new Random().nextInt(10);
		}
		return captcha;
	}
	
	
	/**
	 * @description 获取两个日期之间相差天数
	 * @author xudj
	 * @param fDate
	 * @param oDate
	 * @return
	 */
	public static int getDateDiff(Date fDate, Date oDate) {
		 if (null == fDate || null == oDate) {
	           return -1;
	       }
	       long intervalMilli = oDate.getTime() - fDate.getTime();
	       return (int)(intervalMilli / (24 * 60 * 60 * 1000));
	}

	/**
	 * 获取两个日期之间相差的小时
	 * @return
	 */
	public static String getHourDiff(Date fDate, Date oDate) {
		if (null == fDate || null == oDate) {
			return "";
		}
		long intervalMilli = oDate.getTime() - fDate.getTime();
		if(intervalMilli <= 0){
			return "";
		}
		int days = (int) (intervalMilli / (24 * 60 * 60 * 1000));
		int hours = (int) (intervalMilli / (60 * 60 * 1000)) % 24;
		String dateHoursDiff = "";
		if(days != 0){
			dateHoursDiff = days + "天"; 
		}
		if(hours != 0){
			dateHoursDiff = dateHoursDiff + hours + "小时";
		}
		if(dateHoursDiff.equals("")){
			return "1小时内";
		}
		return dateHoursDiff;
	}
	
	/**
	 * @description 根据创建时间获取发布距离现在多久
	 * @author xudj
	 * @param createTime
	 * @return
	 */
	public static String getPublishTime(Date createTime){
	    long time1=createTime.getTime();
	
	    long time2=new Date().getTime(); 
	    long resultTime=(time2-time1)/1000/60;
	    String publishTime="";
	    if (resultTime<10) {
			publishTime="刚刚";
		}else if (resultTime>=10&&resultTime<60) {
			publishTime=resultTime/10+"0分钟前";
		}else if (resultTime>60&&resultTime<1440) {
			publishTime=resultTime/60+"小时前";
		}else if (resultTime>1440&&resultTime<2880) {
			publishTime="昨天";
		}else{
			publishTime= dateFormat_yyyy_MM_dd.format(createTime);
		}
	    return publishTime;

	}	

	/**
	 * @description 获得UUID，去中划线后转为大写
	 * @author xudj
	 * @return
	 */
	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		String guid = uuid.toString().replace("-", "").toUpperCase();
		return guid;
	}
	
	/**
	 * @description 将数据转换为Integer类型，null的情况下处理成0
	 * @author xudj
	 * @param value
	 * @return
	 */
	public static Integer parseInteger(Object value){
		if(hasText(value)){
			return Integer.valueOf(value.toString());
		}else{
			return 0;
		}
	}
	
	/**
	 * @description 获取当前网络ip(主要处理反向代理时无法获取真实ip的问题)
	 * @author xudj
	 * @param request
	 * @return
	 */
    public static String getIpAddress(HttpServletRequest request){  
        String ipAddress = request.getHeader("x-forwarded-for");  
            if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
                ipAddress = request.getHeader("Proxy-Client-IP");  
            }  
            if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
                ipAddress = request.getHeader("WL-Proxy-Client-IP");  
            }  
            if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {  
                ipAddress = request.getRemoteAddr();  
                if(ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")){  
                    //根据网卡取本机配置的IP  
                    InetAddress inet=null;  
                    try {  
                        inet = InetAddress.getLocalHost();  
                    } catch (UnknownHostException e) {
                        e.printStackTrace();  
                        return "";
                    }  
                    ipAddress= inet.getHostAddress();  
                }  
            }  
            //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割  
            if(ipAddress!=null && ipAddress.length()>15){ //"***.***.***.***".length() = 15  
                if(ipAddress.indexOf(",")>0){  
                    ipAddress = ipAddress.substring(0,ipAddress.indexOf(","));  
                }  
            }  
            return ipAddress;   
    }
    
    final static char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
		'9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
		'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
		'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
		'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
		'Z' };
	/**
	 * 随机ID生成器，由数字、小写字母和大写字母组成
	 * @author xudj20170213
	 * @param size
	 * @return
	 */
	public static String getRandomNum(int size) {
		Random random = new Random();
		char[] cs = new char[size];
		for (int i = 0; i < cs.length; i++) {
			cs[i] = digits[random.nextInt(digits.length)];
		}
		return new String(cs);
	}
	
	/**
	 * 4个直辖市
	 */
	public static List<String> municipalitys(){
		List<String> municipalitys = new ArrayList<String>();
		municipalitys.add("北京市");
		municipalitys.add("天津市");
		municipalitys.add("天津市");
		municipalitys.add("重庆市");
		return municipalitys;
	}
}
