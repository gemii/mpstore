package cc.gemii.po;

import java.util.Date;

public class MpImages {
    private Long imgId;

    private String imgUrl;

    private String imgTitle;

    private String imgInfo;

    private Double imgWidth;

    private Double imgHeight;

    private Date createTime;

    private Date updateTime;

    private Byte status;

    public Long getImgId() {
        return imgId;
    }

    public void setImgId(Long imgId) {
        this.imgId = imgId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    public String getImgTitle() {
        return imgTitle;
    }

    public void setImgTitle(String imgTitle) {
        this.imgTitle = imgTitle == null ? null : imgTitle.trim();
    }

    public String getImgInfo() {
        return imgInfo;
    }

    public void setImgInfo(String imgInfo) {
        this.imgInfo = imgInfo == null ? null : imgInfo.trim();
    }

    public Double getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(Double imgWidth) {
        this.imgWidth = imgWidth;
    }

    public Double getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(Double imgHeight) {
        this.imgHeight = imgHeight;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}