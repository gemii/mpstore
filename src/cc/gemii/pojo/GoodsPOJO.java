package cc.gemii.pojo;

/**
 * 商品API数据
 * 
 * @author xudj
 */
public class GoodsPOJO {

	private String goods_code;		//乐其商品商家编码
	private String goods_name;		//商品名称
	private Integer goods_number;	//商品数量
	private Float goods_price;		//商品单价
	private String barcode;			//商品条码

	public String getGoods_code() {
		return goods_code;
	}

	public void setGoods_code(String goods_code) {
		this.goods_code = goods_code;
	}

	public String getGoods_name() {
		return goods_name;
	}

	public void setGoods_name(String goods_name) {
		this.goods_name = goods_name;
	}

	public Integer getGoods_number() {
		return goods_number;
	}

	public void setGoods_number(Integer goods_number) {
		this.goods_number = goods_number;
	}

	public Float getGoods_price() {
		return goods_price;
	}

	public void setGoods_price(Float goods_price) {
		this.goods_price = goods_price;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

}
