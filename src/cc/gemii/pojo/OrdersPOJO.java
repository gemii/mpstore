package cc.gemii.pojo;

import java.util.Date;
import java.util.List;

/**
 * 订单API数据
 * 
 * @author xudj
 */
public class OrdersPOJO {

	private String order_id;		//订单号
	private String order_amount;	//订单总额（顾客实际支付金额）
	private Float shipping_fee;		//运费
	private Float bonus;			//优惠券金额
	private String nick_name;		//会员名
	private String pay_name;		//支付方式
	private String consignee;		//客户姓名
	private String province;		//省
	private String city;			//市
	private String district;		//区
	private String address;			//收货地址
	private String mobile;			//手机号
	private String order_type_id;	//订单类型
	private Date order_time;		//付款时间
	private Date pay_time;			//付款时间
	private List<GoodsPOJO> goods;	//商品信息

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(String order_amount) {
		this.order_amount = order_amount;
	}

	public Float getShipping_fee() {
		return shipping_fee;
	}

	public void setShipping_fee(Float shipping_fee) {
		this.shipping_fee = shipping_fee;
	}

	public Float getBonus() {
		return bonus;
	}

	public void setBonus(Float bonus) {
		this.bonus = bonus;
	}

	public String getNick_name() {
		return nick_name;
	}

	public void setNick_name(String nick_name) {
		this.nick_name = nick_name;
	}

	public String getPay_name() {
		return pay_name;
	}

	public void setPay_name(String pay_name) {
		this.pay_name = pay_name;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getOrder_type_id() {
		return order_type_id;
	}

	public void setOrder_type_id(String order_type_id) {
		this.order_type_id = order_type_id;
	}

	public Date getOrder_time() {
		return order_time;
	}

	public void setOrder_time(Date order_time) {
		this.order_time = order_time;
	}

	public Date getPay_time() {
		return pay_time;
	}

	public void setPay_time(Date pay_time) {
		this.pay_time = pay_time;
	}

	public List<GoodsPOJO> getGoods() {
		return goods;
	}

	public void setGoods(List<GoodsPOJO> goods) {
		this.goods = goods;
	}

}
