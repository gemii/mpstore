package cc.gemii.pojo;



/**
 * 自定义响应结构
 */
public class CommonResult {

    // 响应业务状态
    private Integer code;

    // 响应消息
    private String msg;

    // 响应中的数据
    private Object data;
    
    //数据量
    private Integer count;

    public CommonResult() {

    }
    
    public static CommonResult ok(Object data, Integer count) {
        return new CommonResult(data, count);
    }

    public static CommonResult ok() {
        return new CommonResult(null, 0);
    }

    public static CommonResult build(Integer code, String msg, Object data, Integer count) {
        return new CommonResult(code, msg, data, count);
    }

    public static CommonResult build(Integer code, String msg) {
        return new CommonResult(code, msg, null, 0);
    }

    public CommonResult(Integer code, String msg, Object data, Integer count) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }

    public CommonResult(Object data, Integer count) {
        this.code = 0;
        this.msg = "ok";
        this.data = data;
        this.count = count;
    }

    public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
