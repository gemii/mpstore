package cc.gemii.pojo;

/**
 * @description 分页
 * @author xudj
 * @date 20170329
 * @version 1.0
 */
public class PagingPOJO {
	private Integer pageNo;	//当前请求页
	private Integer pageSize;	//请求页大小
	private Integer pageStart;	//开始记录数
	private Integer pageTotal;	//查询的总记录数
	private Integer type;   //0  倒序  1  正序
	public Integer getPageNo() {
		if(null == pageNo){
			pageNo = 1;
		}else if(pageNo < 1){
			pageNo = 1;
		}
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		if(null==pageSize){
			pageSize = 5;
		}
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageStart() {
		setPageStart((getPageNo() - 1)*getPageSize());
		return pageStart;
	}
	public void setPageStart(Integer pageStart) {
		this.pageStart = pageStart;
	}
	public Integer getPageTotal() {
		return pageTotal;
	}
	public void setPageTotal(Integer pageTotal) {
		this.pageTotal = pageTotal;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
}
