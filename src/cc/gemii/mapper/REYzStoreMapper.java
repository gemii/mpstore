package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.pojo.GoodsPOJO;
import cc.gemii.pojo.OrdersPOJO;

import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult.TradeDetailV2;
import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult.TradeOrderV2;


public interface REYzStoreMapper {

	void insertTradesSoldGet(TradeDetailV2 tradeDetailV22);

	int selectOrderCountByTradesId(String tid);

	void updateTradeOrder(@Param("tradeOrder")TradeOrderV2 tradeOrderV22, @Param("tid")String tid);

	List<OrdersPOJO> getOrders(@Param("startTime") String startTime, @Param("endTime") String endTime);

	List<GoodsPOJO> getGoodsByTradesId(String order_id);

}
