package cc.gemii.mapper;

import java.util.Map;


public interface UserCustomMapper {

	void createSession(Map<String, Object> param);

	String getSessionKey(String session_id);

	void insertUser(Map<String, Object> user);
}