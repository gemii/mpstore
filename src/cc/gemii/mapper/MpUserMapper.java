package cc.gemii.mapper;

import cc.gemii.po.MpUser;
import cc.gemii.po.MpUserExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface MpUserMapper {
    int countByExample(MpUserExample example);

    int deleteByExample(MpUserExample example);

    int deleteByPrimaryKey(String openId);

    int insert(MpUser record);

    int insertSelective(MpUser record);

    List<MpUser> selectByExample(MpUserExample example);

    MpUser selectByPrimaryKey(String openId);

    int updateByExampleSelective(@Param("record") MpUser record, @Param("example") MpUserExample example);

    int updateByExample(@Param("record") MpUser record, @Param("example") MpUserExample example);

    int updateByPrimaryKeySelective(MpUser record);

    int updateByPrimaryKey(MpUser record);

}