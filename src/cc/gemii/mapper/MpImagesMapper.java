package cc.gemii.mapper;

import cc.gemii.po.MpImages;
import cc.gemii.po.MpImagesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MpImagesMapper {
    int countByExample(MpImagesExample example);

    int deleteByExample(MpImagesExample example);

    int deleteByPrimaryKey(Long imgId);

    int insert(MpImages record);

    int insertSelective(MpImages record);

    List<MpImages> selectByExample(MpImagesExample example);

    MpImages selectByPrimaryKey(Long imgId);

    int updateByExampleSelective(@Param("record") MpImages record, @Param("example") MpImagesExample example);

    int updateByExample(@Param("record") MpImages record, @Param("example") MpImagesExample example);

    int updateByPrimaryKeySelective(MpImages record);

    int updateByPrimaryKey(MpImages record);
}