package cc.gemii.mapper;

import java.util.List;

import cc.gemii.pojo.MpImageVO;
import cc.gemii.pojo.PagingPOJO;

public interface REMpImagesMapper {

	List<MpImageVO> listMpImage(PagingPOJO pagingPOJO);

}
