package cc.gemii.mapper;

import cc.gemii.po.MpEventLog;
import cc.gemii.po.MpEventLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MpEventLogMapper {
    int countByExample(MpEventLogExample example);

    int deleteByExample(MpEventLogExample example);

    int deleteByPrimaryKey(String openId);

    int insert(MpEventLog record);

    int insertSelective(MpEventLog record);

    List<MpEventLog> selectByExample(MpEventLogExample example);

    MpEventLog selectByPrimaryKey(String openId);

    int updateByExampleSelective(@Param("record") MpEventLog record, @Param("example") MpEventLogExample example);

    int updateByExample(@Param("record") MpEventLog record, @Param("example") MpEventLogExample example);

    int updateByPrimaryKeySelective(MpEventLog record);

    int updateByPrimaryKey(MpEventLog record);
}