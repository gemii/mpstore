package cc.gemii.service;

import java.util.Map;

import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult.TradeDetailV2;

public interface YzStoreService {

	void handTradesSoldGet(TradeDetailV2 tradeDetailV22);

	Map<String, Object> getOrders(String startTime, String endTime);

}
