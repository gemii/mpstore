package cc.gemii.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.REYzStoreMapper;
import cc.gemii.pojo.GoodsPOJO;
import cc.gemii.pojo.OrdersPOJO;
import cc.gemii.service.YzStoreService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.EmojisUtil;

import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult.TradeDetailV2;
import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult.TradeOrderV2;
import com.youzan.open.sdk.util.json.JsonUtils;

/**
 * 有赞商城业务
 * @author xudj
 */
@Service
public class YzStoreServiceImpl implements YzStoreService {
	
	@Autowired
	private REYzStoreMapper reYzStoreMapper;

	/**
	 * 解析订单信息
	 * @author xudj20170331
	 */
	@Override
	public void handTradesSoldGet(TradeDetailV2 tradeDetailV22) {
		System.out.println(JsonUtils.toJson(tradeDetailV22));
		//处理昵称fans_nickName
		String nackName = EmojisUtil.resolveToByteFromEmoji(tradeDetailV22.getFansInfo().getFansNickname());
		tradeDetailV22.getFansInfo().setFansNickname(nackName);
		//插入订单数据
		reYzStoreMapper.insertTradesSoldGet(tradeDetailV22);
		TradeOrderV2[] tradeOrderV2 = tradeDetailV22.getOrders();
		for (TradeOrderV2 tradeOrderV22 : tradeOrderV2) {
			//能查询到则不处理
			reYzStoreMapper.updateTradeOrder(tradeOrderV22, tradeDetailV22.getTid());
		}
	}

	/**
	 * 乐其获取订单信息
	 * @author xudj20170331
	 */
	@Override
	public Map<String, Object> getOrders(String startTime, String endTime) {
		Map<String, Object> res = new HashMap<String, Object>();
		try {
			List<OrdersPOJO> orders = reYzStoreMapper.getOrders(startTime, endTime);
			for (OrdersPOJO ordersPOJO : orders) {
				//处理会员名－昵称
				ordersPOJO.setNick_name(EmojisUtil.resolveToEmojiFromByte(ordersPOJO.getNick_name()));
				
				//处理省市信息
				this.handProvince(ordersPOJO);
				
				//查询订单下的商品
				List<GoodsPOJO> goods = reYzStoreMapper.getGoodsByTradesId(ordersPOJO.getOrder_id());
				ordersPOJO.setGoods(goods);
			}
			res.put("result", "success");
			res.put("code", 200);
			res.put("data", orders);
		} catch (Exception e) {
			res.put("result", "fail");
			res.put("code", 40005);
		}
		return res;
	}

	/**
	 * 处理城市信息
	 * @author xudj20170321
	 * @param ordersPOJO
	 */
	private void handProvince(OrdersPOJO ordersPOJO) {
		String province = ordersPOJO.getProvince();	//省
		String city = ordersPOJO.getCity();	//市
		String district = ordersPOJO.getDistrict();	//区
		//去除省市的最后一个字段
		if(province != null && province.lastIndexOf("省") != -1){
			ordersPOJO.setProvince(province.substring(0, province.length()-1));
		}
		if(city != null && city.lastIndexOf("市") != -1){
			ordersPOJO.setCity(city.substring(0, city.length()-1));
		}
		//直辖市特殊处理
		if(CommonUtil.municipalitys().contains(city)){
			ordersPOJO.setProvince(city.substring(0, city.length()-1));//省变市
			ordersPOJO.setCity(district);//市变区
			ordersPOJO.setDistrict(null);//区去除
		}
	}

}
