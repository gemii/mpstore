package cc.gemii.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cc.gemii.fuckemoji.EmojiUtil;
import cc.gemii.mapper.MpEventLogMapper;
import cc.gemii.mapper.MpImagesMapper;
import cc.gemii.mapper.MpUserMapper;
import cc.gemii.mapper.REMpImagesMapper;
import cc.gemii.mapper.UserCustomMapper;
import cc.gemii.po.MpEventLog;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.MpImageVO;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.service.MpService;
import cc.gemii.utils.AESUtil;
import cc.gemii.utils.HttpClientUtil;
import cc.gemii.utils.UrlConsts;

import com.alibaba.fastjson.JSON;

/**
 * 小程序接口业务
 * @author xudj20170329
 */
@Service
public class MpServiceImpl implements MpService {

	@Autowired
	private MpImagesMapper mpImagesMapper;

	@Autowired
	private REMpImagesMapper reMpImagesMapper;
	
	@Autowired
	private MpUserMapper mpUserMapper;
	
	@Autowired
	private MpEventLogMapper eventLogMapper;
	
	@Autowired
	private UserCustomMapper userMapper;
	
	@Value("${MINI_APP_ID}")
	private String mini_app_id;
	
	@Value("${MINI_APP_SECRET}")
	private String mini_app_secret;
	
	/**
	 * 查询图片列表
	 */
	@Override
	public CommonResult listMpImage(PagingPOJO pagingPOJO) {
		List<MpImageVO> list = reMpImagesMapper.listMpImage(pagingPOJO);
		int count = mpImagesMapper.countByExample(null);
		return CommonResult.ok(list, count);
	}

	/**
	 * 增加日志
	 */
	@Override
	public String insertMpEventLog(MpEventLog eventLog) {
		int count = eventLogMapper.insertSelective(eventLog);
		if(count == 0){
			return "fail";
		}
		return "success";
	}

	@Override
	public Map<String, Object> login(String code, String session_id) {
		String url = UrlConsts.GET_OPENID_SK_URL.replace("MINI_APP_ID", mini_app_id).replace("MINI_APP_SECRET", mini_app_secret).replace("MINI_CODE", code);
		String req_result = HttpClientUtil.doGet(url);
		//response
		Map<String, Object> resp = new HashMap<String, Object>();

		Map<String, String> req_map = (Map<String, String>) JSON.parse(req_result);
		if(req_map.containsKey("errcode")){
			resp.put("status", -1);
			resp.put("msg", "login failed");
		}else{
			String openid = req_map.get("openid");
			String session_key = req_map.get("session_key");
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("openid", openid);
			param.put("session_key", session_key);
			param.put("session_id", session_id);
			userMapper.createSession(param);
			resp.put("status", 200);
			resp.put("msg", "ok");
		}
		
		return resp;
	}

	@Override
	public Map<String, Object> getUserInfo(String session_id,
			String encryptedData, String iv) {
		Map<String, Object> resp = new HashMap<String, Object>();
		try {
			String session_key = userMapper.getSessionKey(session_id);
			byte[] session_key_bytes = AESUtil.decode(session_key.getBytes());
			byte[] encryptedData_bytes = AESUtil.decode(encryptedData.getBytes());
			byte[] iv_bytes = AESUtil.decode(iv.getBytes());
			byte[] userDataBytes = AESUtil.decrypt(encryptedData_bytes, session_key_bytes, iv_bytes);
			String userData = new String(userDataBytes, "utf-8");
			Map<String, Object> userMap = (Map<String, Object>) JSON.parse(userData);
			this.handleUserInfo(userMap);
			resp.put("status", 200);
			resp.put("msg", "ok");
			resp.put("data", userMap);
			return resp;
		} catch (Exception e) {
			e.printStackTrace();
			resp.put("status", -1);
			resp.put("msg", "server error");
			resp.put("data", null);
			return resp;
		}
	}

	private void handleUserInfo(Map<String, Object> user){
		Map<String, Object> watermark = (Map<String, Object>) user.get("watermark");
		Integer timestamp = (Integer) watermark.get("timestamp");
		Date timestampDate = new Date(Long.valueOf(timestamp+"000"));
		user.put("timestamp", timestampDate);
		user.put("nickName", EmojiUtil.handleEmoji(String.valueOf(user.get("nickName"))));
		userMapper.insertUser(user);
	}
}
