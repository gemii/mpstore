package cc.gemii.service;

import java.util.Map;

import cc.gemii.po.MpEventLog;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;

public interface MpService {

	CommonResult listMpImage(PagingPOJO pagingPOJO);

	String insertMpEventLog(MpEventLog eventLog);

	Map<String, Object> login(String code, String session_id);

	Map<String, Object> getUserInfo(String session_id, String encryptedData,
			String iv);

}
