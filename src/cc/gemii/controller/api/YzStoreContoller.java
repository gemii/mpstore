package cc.gemii.controller.api;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.service.YzStoreService;
import cc.gemii.utils.CommonUtil;
import cc.gemii.utils.MD5Utils;

/**
 * 乐其接口对接
 * @author xudj
 */
@Controller
@RequestMapping("/store")
public class YzStoreContoller {
	
	private static final Logger log = LoggerFactory.getLogger(YzStoreContoller.class);

	@Autowired
	private YzStoreService yzStoreService;
	@Value("${LEQEE_USER}")
	private String LEQEE_USER;
	@Value("${LEQEE_IP}")
	private String LEQEE_IP;
	@Value("${LEQEE_TOKEN}")
	private String LEQEE_TOKEN;
	
	/**
	 * 乐其拉取订单信息
	 * @param startTime：开始时间
	 * @param endTime：结束时间
	 * @param user：来源
	 * @param sign：签名
	 * @param request
	 * @return
	 */
	@RequestMapping("/getOrders")
	@ResponseBody
	public Map<String, Object> getOrders(@RequestParam String startTime, @RequestParam String endTime, @RequestParam String user, @RequestParam String sign, HttpServletRequest request){
		if(log.isDebugEnabled()){
			log.debug("startTime:{}, endTime:{}, user:{}, sign:{}", startTime, endTime, user, sign);
		}
		//ip校验及身份校验
		Map<String, Object> map = this.verifyIPAndUser(user, request);
		if(map != null){
			return map;
		}
		//检验签名
		map = this.verifySign(startTime, endTime, sign);
		if(map != null){
			return map;
		}
		//查询接口
		return yzStoreService.getOrders(startTime, endTime);
	}

	/**
	 * 检验签名
	 * @param startTime
	 * @param endTime
	 * @param sign
	 * @return
	 */
	private Map<String, Object> verifySign(String startTime, String endTime, String sign) {
		Map<String, Object> res = new HashMap<String, Object>();
		String startTimeStr = startTime.replace("-", "").replace(" ", "").replace(":", "");//去-和空格
		String endTimeStr = endTime.replace("-", "").replace(" ", "").replace(":", "");//去-和空格
		String originStr = LEQEE_USER + startTimeStr + endTimeStr + LEQEE_TOKEN;
		String MD5Str = MD5Utils.md5Encode(originStr);
		//日志
		log.info("originStr:{}, sign:{}", originStr, MD5Str);
		if(!sign.equals(MD5Str)){//签名失败
			res.put("result", "fail");
			res.put("code", 40004);
			return res;
		}
		return null;
	}

	/**
	 * ip及身份校验
	 * @param request
	 * @return
	 */
	private Map<String, Object> verifyIPAndUser(String user, HttpServletRequest request) {
		String requestIp = CommonUtil.getIpAddress(request);//获取ip
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("result", "fail");
		log.info(requestIp);
		if(!CommonUtil.hasText(requestIp)){//ip获取失败
			res.put("code", 40001);
			return res;
		}
//		if(!requestIp.equals(LEQEE_IP)){//ip不匹配
//			res.put("code", 40002);
//			return res;
//		}
		if(!LEQEE_USER.equals(user)){//来源校验
			res.put("code", 40003);
			return res;
		}
		//通过
		return null;
	}
	
}
