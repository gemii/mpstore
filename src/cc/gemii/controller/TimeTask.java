package cc.gemii.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cc.gemii.service.YzStoreService;

import com.youzan.open.sdk.client.auth.Sign;
import com.youzan.open.sdk.client.core.DefaultKDTClient;
import com.youzan.open.sdk.client.core.KDTClient;
import com.youzan.open.sdk.gen.v2_0_0.api.KdtTradesSoldGet;
import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetParams;
import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult;
import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult.TradeDetailV2;

/**
 * 定时任务
 * @author xudj20170213
 */
@Component
public class TimeTask {
	//日志
	private static final Logger log = LoggerFactory.getLogger(TimeTask.class);
	
	@Value("${APP_ID}")
	private String APP_ID;
	@Value("${APP_SECRET}")
	private String APP_SECRET;
	
	@Autowired
	private YzStoreService yzStoreService;
	
	/**
	 * 下午3点拉取订单数据
	 * cron："0 0 15 * * ?"
	 * @author xudj20170330
	 */
//	@Scheduled(cron = "0 */1 * * * ?")
	public void tasksKdtTradesSoldGet(){
		log.info("查询卖家已卖出的交易列表 start");
		//定义请求client
	    KDTClient client = new DefaultKDTClient(new Sign(APP_ID, APP_SECRET)); //new Token("xxxx")
	    //封装参数
	    KdtTradesSoldGetParams kdtTradesSoldGetParams =new KdtTradesSoldGetParams();
	    kdtTradesSoldGetParams.setPageNo(1L);
	    kdtTradesSoldGetParams.setPageSize(20L);
	    //...

	    //创建API
	    KdtTradesSoldGet kdtTradesSoldGet = new KdtTradesSoldGet();
	    //给API设置参数
	    kdtTradesSoldGet.setAPIParams(kdtTradesSoldGetParams);
	    //请求数据方法
	    doTradesSoldGet(client, kdtTradesSoldGetParams, kdtTradesSoldGet);
	    
	    log.info("查询卖家已卖出的交易列表 end");
	}
	
	/**
	 * 递归发起数据请求
	 * @param client
	 * @param kdtTradesSoldGetParams：请求参数
	 * @param kdtTradesSoldGet
	 */
	public void doTradesSoldGet(KDTClient client, KdtTradesSoldGetParams kdtTradesSoldGetParams, KdtTradesSoldGet kdtTradesSoldGet){
	    //client发起请求——返回调用结果
		KdtTradesSoldGetResult result = client.invoke(kdtTradesSoldGet);
//		System.out.println(JsonUtils.toJson(result));
		
		TradeDetailV2[] tradeDetailV2 = result.getTrades();
	    for (TradeDetailV2 tradeDetailV22 : tradeDetailV2) {
	    	//处理获取的订单数据
	    	yzStoreService.handTradesSoldGet(tradeDetailV22);
		}
	    //分页加载下一页
	    long totalResults = result.getTotalResults();
	    long pageNo = kdtTradesSoldGetParams.getPageNo();
	    long pageSize = kdtTradesSoldGetParams.getPageSize();
	    if(totalResults > pageNo * pageSize){
	    	kdtTradesSoldGetParams.setPageNo(pageNo + 1);
	    	doTradesSoldGet(client, kdtTradesSoldGetParams, kdtTradesSoldGet);
	    }
	}

}
