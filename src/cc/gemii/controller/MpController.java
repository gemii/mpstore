package cc.gemii.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.MpEventLog;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.PagingPOJO;
import cc.gemii.service.MpService;
import cc.gemii.utils.CommonUtil;

import com.alibaba.fastjson.JSON;

/**
 * 小程序接口
 * @author xudj20170329
 */
@Controller
@RequestMapping("/mp")
public class MpController {
	private static final Logger log = LoggerFactory.getLogger(MpController.class);

	@Autowired
	private MpService mpService;
	
	/**
	 * 查询集合
	 * @return
	 */
	@RequestMapping("/image/list")
	@ResponseBody
	public CommonResult list(PagingPOJO pagingPOJO){
		return mpService.listMpImage(pagingPOJO);
	}
	
	/**
	 * 增加访问记录
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="log", method=RequestMethod.POST)
	@ResponseBody
	public String detail(HttpServletRequest request){
		MpEventLog eventLog = new MpEventLog();
		try {
			String jsonString = IOUtils.toString(request.getInputStream());
			if(log.isDebugEnabled()){
				log.debug(jsonString);
			}
			Map<String, Object> map = (Map<String, Object>) JSON.parse(jsonString);
			eventLog.setOpenId((String)map.get("openId"));
			eventLog.setItemId(map.get("itemId")+"");
			eventLog.setEventType(map.get("eventType")==null?"default":(String)map.get("eventType"));
		} catch (IOException e) {
			e.printStackTrace();
			return "error";
		}
		return mpService.insertMpEventLog(eventLog);
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> login(HttpServletRequest request){
		try {
			String jsonString = IOUtils.toString(request.getInputStream());
			Map<String, String> map = (Map<String, String>) JSON.parse(jsonString); 
			String code = map.get("code");
			String session_id = map.get("session_id");
			return mpService.login(code, session_id);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getUserInfo(HttpServletRequest request){
		try {
			String jsonString = IOUtils.toString(request.getInputStream());
			Map<String, String> map = (Map<String, String>) JSON.parse(jsonString); 
			String session_id = map.get("session_id");
			String encryptedData = map.get("encryptedData");
			String iv = map.get("iv");
			//非空校验
			if(!CommonUtil.hasText(session_id) || !CommonUtil.hasText(encryptedData) || !CommonUtil.hasText(iv)){
				Map<String, Object> result = new HashMap<String, Object>();
				result.put("status", 500);
				result.put("msg", "参数异常");
				return result;
			}
			return mpService.getUserInfo(session_id, encryptedData, iv);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
