package test;

import com.youzan.open.sdk.client.auth.Sign;
import com.youzan.open.sdk.client.core.DefaultKDTClient;
import com.youzan.open.sdk.client.core.KDTClient;
import com.youzan.open.sdk.gen.v1_0_0.api.KdtItemsOnsaleGet;
import com.youzan.open.sdk.gen.v1_0_0.model.KdtItemsOnsaleGetParams;
import com.youzan.open.sdk.gen.v1_0_0.model.KdtItemsOnsaleGetResult;
import com.youzan.open.sdk.gen.v2_0_0.api.KdtTradesSoldGet;
import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetParams;
import com.youzan.open.sdk.gen.v2_0_0.model.KdtTradesSoldGetResult;
import com.youzan.open.sdk.util.json.JsonUtils;

public class ApiTest {

	private static final String APP_ID = "bd292516bdc5792e6b";
	private static final String APP_SECRET = "9de0c2d856f5d0b1e7edfc7e743ee567";
	
	public static void main(String[] args) {
		
		testTradesSold();
//		testItemsOnsaleGet();
	}
	
	public static void testTradesSold() {
		//定义请求client
	    KDTClient client = new DefaultKDTClient(new Sign(ApiTest.APP_ID, ApiTest.APP_SECRET)); //new Token("xxxx")

	    //封装参数
	    KdtTradesSoldGetParams kdtTradesSoldGetParams =new KdtTradesSoldGetParams();
	    kdtTradesSoldGetParams.setPageNo(1L);
	    kdtTradesSoldGetParams.setPageSize(30L);

	    //...

	    //创建API
	    KdtTradesSoldGet kdtTradesSoldGet = new KdtTradesSoldGet();
	    //给API设置参数
	    kdtTradesSoldGet.setAPIParams(kdtTradesSoldGetParams);

	    //client发起请求——返回调用结果
	    KdtTradesSoldGetResult result = client.invoke(kdtTradesSoldGet);

	    System.out.println(JsonUtils.toJson(result));
	}
	
	public static void testItemsOnsaleGet(){
		//定义请求client
	    @SuppressWarnings("resource")
		KDTClient client = new DefaultKDTClient(new Sign(APP_ID, APP_SECRET)); //new Token("xxxx")
	    
	    KdtItemsOnsaleGetParams kdtItemsOnsaleGetParams = new KdtItemsOnsaleGetParams();
	    KdtItemsOnsaleGet kdtItemsOnsaleGet = new KdtItemsOnsaleGet();
	    kdtItemsOnsaleGet.setAPIParams(kdtItemsOnsaleGetParams);

	    kdtItemsOnsaleGetParams.setPageSize(20L);
	    kdtItemsOnsaleGetParams.setPageNo(1L);
	    kdtItemsOnsaleGetParams.setFields("title,desc");
	    kdtItemsOnsaleGetParams.setOrderBy("created");

	    KdtItemsOnsaleGetResult result = client.invoke(kdtItemsOnsaleGet);
//	    System.out.println(result.getItems()[0].toString());
	    System.out.println(JsonUtils.toJson(result));
	}
}
